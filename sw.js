const staticCacheName = 'site-static-v1';
const dynamicCacheName = 'site-dynamic-v1';
const assets = [
    '/',
    'first.html',
    'styles.css',
    'functions.js',
    'manifest.json',
    'fallback_page.html',
    'dashboard.html',
    'tasks.html',
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'
];

// Install service worker 
self.addEventListener('install', evt => {
    evt.waitUntil(
        caches.open(staticCacheName).then(cache => {
            console.log('Caching shell assets');
            cache.addAll(assets);
        })
    );
});

//Activate event
self.addEventListener('activate', evt => {
    evt.waitUntil(
        caches.keys().then(keys => {
            return Promise.all(keys
                .filter(key => key !== staticCacheName)
                .map(key => caches.delete(key))
            )
        })
    );
});

//Fetch event 
self.addEventListener('fetch', event => {
    event.respondWith(caches.open(dynamicCacheName).then( async cache => {
        const staticCache = await caches.open(staticCacheName)
        const staticCachedReponse = await staticCache.match(event.request.url)
        if(!staticCachedReponse) {
            const cachedResponse = await cache.match(event.request.url);
            if (cachedResponse) {
                return cachedResponse;
            }
            try {
                const fetchedResponse = await fetch(event.request);
                // Add the network response to the cache for later visits
                cache.put(event.request, fetchedResponse.clone());
                return fetchedResponse;
            } catch (e) {
                return staticCache.match('fallback_page.html')
            }
        } else {
            return staticCachedReponse
        }
    }))
});
