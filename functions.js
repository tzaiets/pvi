let ArrayOfStudents = []
let edit = null;
let indexStudent = 0;

const openAddStudPopUpBtn = document.getElementById("add-student-btn");
const closeAddEditStudPopUpBtn = document.getElementById("add-edit-form-close-btn");
const addEditStudPopUp = document.getElementById("pop-up-add-edit-student");
const okAddEditStudPopUpBtn = document.getElementById("ok-add-edit-student-btn");
const addEditStudPopUpBtn = document.getElementById("create-edit-student-btn");
const addEditStudForm = document.getElementById("form-add-edit-student");

const studentsTableId = document.getElementById("students-table-id");
const studentsTableBody = studentsTableId.getElementsByTagName("tbody")[0];

const GroupSelect = document.getElementById("student-groups-select");
const NameInput = document.getElementById("student-first-name");
const SurnameInput = document.getElementById("student-last-name");
const GenderSelect = document.getElementById("student-gender-select");
const BirthdayInput = document.getElementById("student-birthday");

let popUpAddEditTitle = document.getElementById("pop-up-h3");
let CheckboxesArray = document.getElementsByClassName("checkboxes");
let CheckedCheckboxCounter = 0;

const deleteStudPopUp = document.getElementById("pop-up-delete");
const okDeleteBtn = document.getElementById("ok-delete-btn");
const closeStudPopUp = document.getElementById("delete-form-close-btn");
const cancelStudPopUp = document.getElementById("cancel-delete-btn");

// Clear studAddEdit form fields after clicking on Submit button
function clearStudAddFormFields() {
    GroupSelect.value = "";
    NameInput.value = "";
    SurnameInput.value = "";
    GenderSelect.value = "";
    BirthdayInput.value = "";
} 

// Adding students section
openAddStudPopUpBtn.addEventListener('click', function(e) {
    console.log("Open form");
    e.preventDefault();
    popUpAddEditTitle.innerHTML = "Add student";
    addEditStudPopUpBtn.value = "Create";
    addEditStudPopUp.classList.add('active');
    
})


closeAddEditStudPopUpBtn.addEventListener('click', ()=> {
    addEditStudPopUp.classList.remove('active');
    clearStudAddFormFields();
})


// Adding a new empty table row when click 'Ok' button
okAddEditStudPopUpBtn.addEventListener('click', function(e) {
    // e.preventDefault();
    console.log("OK click");
    if(popUpAddEditTitle.innerHTML === "Add student")
    {
        studentsTableBody.innerHTML += `
        <tr>
            <td><input type="checkbox" class="student-table-checkbox" class="checkbox-student" name="" value=""></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><i class="fa fa-circle active-user"></i></td>
            <td>
                <button class="edit-student-btn" id="edit-student-btn-id">
                <i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px;"></i>
                </button>
                <button class="delete-student-btn" id="delete-student-btn-id">
                <i class="fa fa-remove" aria-hidden="true" style="font-size: 18px;"></i>
                </button>
            </td>
        </tr>
        `;
        ArrayOfStudents.push({
            id: new Date().getTime(),
            group:"",
            name:"",
            surname:"",
            gender:"",
            birthday:""
        });
        //Checkboxes
        addEditStudPopUp.classList.remove('active');
    }
    else if(popUpAddEditTitle.innerHTML === "Edit stident")
    {
        console.log("Edit student");
    }
    renewTableCheckboxes();
    addEditStudPopUp.classList.remove('active');
    clearStudAddFormFields();
})

// Adding a new student when click 'Create' button
addEditStudForm.addEventListener('submit', onAddEditStudent);

function onAddStudent (e){
    console.log("onAddStudent function");
    e.preventDefault();
    const studentGroup = document.getElementById("student-groups-select").value;
    const studentName = document.getElementById("student-first-name").value;
    const studentSurname = document.getElementById("student-last-name").value;
    const studentGender = document.getElementById("student-gender-select").value;
    const studentBirthday = document.getElementById("student-birthday").value;
    const dateList = studentBirthday.split('-');
    const newStudentBirthday = dateList[2] + '.' + dateList[1] + '.' + dateList[0];
    ArrayOfStudents.push({
        id: new Date().getTime(),
        group: studentGroup,
        name: studentName,
        surname: studentSurname,
        gender: studentGender,
        birthday: newStudentBirthday
    });
    studentsTableBody.innerHTML = ""
    ArrayOfStudents.forEach(function(item) 
    {
        studentsTableBody.innerHTML +=`
        <tr data-id=${item.id}>
            <td class="student-table-text"><input type="checkbox" style="student-table-checkbox" class="checkbox-student" name="" value=""></td>
            <td class="student-table-text">${item.group}</td>
            <td class="student-table-text">${item.name} ${item.surname}</td>
            <td class="student-table-text">${item.gender}</td>
            <td class="student-table-text">${item.birthday}</td>
            <td><i class="fa fa-circle active-user"></i></td>
            <td>
                <button class="edit-student-btn" id="edit-student-btn-id">
                <i class="fa fa-pencil" aria-hidden="true" style="font-size: 18px;"></i>
            </button>
            <button class="delete-student-btn" id="delete-student-btn-id">
                <i class="fa fa-remove" aria-hidden="true" style="font-size: 18px;"></i>
            </button>
            </td>
        </tr>
    `;
    })
    renewTableCheckboxes();
    addEditStudPopUp.classList.remove('active');
    clearStudAddFormFields();
}
function onEditStudent (e){
    console.log("onEditStudent function");
    e.preventDefault();
    const studentGroup = document.getElementById("student-groups-select").value;
    const studentName = document.getElementById("student-first-name").value;
    const studentSurname = document.getElementById("student-last-name").value;
    const studentGender = document.getElementById("student-gender-select").value;
    const studentBirthday = document.getElementById("student-birthday").value;
    const dateList = studentBirthday.split('-');
    const newStudentBirthday = dateList[2] + '.' + dateList[1] + '.' + dateList[0];
    ArrayOfStudents[indexStudent] = {
        id: new Date().getTime(),
        group: studentGroup,
        name: studentName,
        surname: studentSurname,
        gender: studentGender,
        birthday: newStudentBirthday
    };
    edit.cells[1].innerHTML = ArrayOfStudents[indexStudent].group;
    edit.cells[2].innerHTML = ArrayOfStudents[indexStudent].name + " " + ArrayOfStudents[indexStudent].surname;
    edit.cells[3].innerHTML = ArrayOfStudents[indexStudent].gender;
    edit.cells[4].innerHTML = ArrayOfStudents[indexStudent].birthday;
    addEditStudPopUp.classList.remove('active');
    clearStudAddFormFields();
}

function onAddEditStudent (e) {
    if(popUpAddEditTitle.innerHTML === "Add student") {
        onAddStudent(e);
    }
    else if (popUpAddEditTitle.innerHTML === "Edit student") {
        onEditStudent(e);
    }
}

function onEditStudRow(e) {
    console.log("onEditStudRow");
    if(e.target.id === 'add-student-btn'){
        return;
    } else if (!e.target.classList.contains('edit-student-btn')){
        return;
    } else if (CheckedCheckboxCounter > 1) {
        return;
    }

    e.preventDefault();
    popUpAddEditTitle.innerHTML = "Edit student";
    addEditStudPopUpBtn.value = "Save";
    addEditStudPopUp.classList.add('active');
    const editBtn = e.target;
    edit = editBtn.closest('tr')
    indexStudent = edit.rowIndex - 1;

    const editGroupSelect = document.getElementById("student-groups-select");
    const editNameInput = document.getElementById("student-first-name");
    const editSurnameInput = document.getElementById("student-last-name");
    const editGenderSelect = document.getElementById("student-gender-select");
    const editBirthdayInput = document.getElementById("student-birthday");

    editGroupSelect.value = ArrayOfStudents[indexStudent].group;
    editNameInput.value = ArrayOfStudents[indexStudent].name;
    editSurnameInput.value = ArrayOfStudents[indexStudent].surname;
    editGenderSelect.value = ArrayOfStudents[indexStudent].gender;
    const dateList = ArrayOfStudents[indexStudent].birthday.split('-');
    const newStudBirthday = dateList[2] + '-' + dateList[1] + '-' + dateList[0];
    editBirthdayInput.value = newStudBirthday;
}

studentsTableBody.addEventListener('click', onEditStudRow);

function onDeleteStudRow(e) {
    if(!e.target.classList.contains('delete-student-btn')){
        return;
    }
    if($("#checkbox-id")[0].checked === true) {
        while(ArrayOfStudents.length > 0){
            ArrayOfStudents.pop();
        }
        studentsTableBody.innerHTML = "";
        $("checkbox-id").prop("checked", false);
        addEditStudPopUp.classList.remove('active');
        return;
    }

    if(CheckedCheckboxCounter > 1) {
        return;
    }
    e.preventDefault();
    deleteStudPopUp.classList.add('active');
    const deleteBtn = e.target;
    const deleteTR = deleteBtn.closest('tr');
    const studData = deleteTR.getElementsByTagName('td');
    const studName = studData[2].innerHTML;
    deleteStudParagraph = document.getElementById("delete-student-p");
    deleteStudParagraph.innerHTML = "Are you sure you want to delete user " + studName + "?";
    okDeleteBtn.addEventListener('click', function(e2){
        indexStudent = deleteTR.rowIndex - 1;
        ArrayOfStudents.splice(indexStudent, 1);
        deleteTR.remove();
        deleteStudPopUp.classList.remove('active');
        CheckedCheckboxCounter = 0;
        addEditStudPopUp.classList.remove('active');
    })
}

studentsTableId.addEventListener('click', onDeleteStudRow);

closeStudPopUp.addEventListener('click', ()=> {
    deleteStudPopUp.classList.remove('active');
})

cancelStudPopUp.addEventListener('click', () => {
    deleteStudPopUp.classList.remove('active');
})

$("#checkbox-id").change(function(){
    if(this.checked === true){
        CheckedCheckboxCounter = 0;
        $(".checkbox-student").each(function(){
            this.checked = true;
        })
        CheckedCheckboxCounter += CheckboxesArray.length - 1;
    }
    else {
        $(".checkbox-student").each(function(){
            this.checked = false;
        })
        CheckedCheckboxCounter -= CheckboxesArray.length - 1;
    }

})

function onCheckboxChange(event) {
    item = event.currentTarget;
    if(item.checked === true){
        CheckedCheckboxCounter += 1;
    }
    else {
        CheckedCheckboxCounter -= 1;
        $("#checkbox-id")[0].checked = false;
    }
    if(CheckedCheckboxCounter === (CheckboxesArray.length - 1)){
        $("#checkbox-id")[0].checked = true;
    }
}

function renewTableCheckboxes() {
    CheckedCheckboxCounter = 0;
    $("#checkbox-id")[0].checked = false;
    for( let i = 1; i < CheckboxesArray.length; i++){
        CheckboxesArray[i].addEventListener("change", onCheckboxChange);
    }
}

if('serviceWorker' in navigator){
    
    navigator.serviceWorker.register('sw.js')
        .then((reg) => console.log('ServiceWorker registred ', reg))
        .catch((err) => console.log('ServiceWorker registration failed', err));
        
}
